#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <complex.h>
#include <float.h>
#include <ctype.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#ifndef lib_h__
#define lib_h__

#define rngtype gsl_rng_ranlxd2
#define M_CUT 8.0
#define A_CUT 3.0

typedef struct
{
    char atm[7];
    int atnum;
    char name[6];
    char res[4];
    char chain[2];
    int resnum;
    double x;
    double y;
    double z;
    double occ;
    double bfac;
    char elem[3];
}
node;//holds node data

typedef struct
{
    node *atoms;
    int N;//no. of nodes
    double G[5];//ddpt outputs: {G0,G11,G12,G2,DDG}
    double T;//temperature
    int iters;//no. of accepected steps to reach
    //Diagnostics//
    int con[2];//lig connectivity: {l1,l2}
    double Is[3][3];//principal axes in columns of Is
    double lig_com[3];//lig COM
    double lig_ax[3];//lig axis

}
ep;//stores system of nodes and it's diagnostics

//lib
double SQR(double x);
double rad(double x,double y,double z);

double urand(gsl_rng *r,double a, double b);
int rsign();
void nrdist(double rdist, double cord);
//--------------------------------------

//SYS utilities
int spawn(char* program,char** arg_list);
void copy(char path[],char dpath[]);
void mv(char path[],char dpath[]);
void ddpt(char fname[]);
void enms(int d);
//-------------

//RW utilities
void cpystr(char dest[],char a[],int len);
int ainb(char a[],char b[]);
void extract(char lign[],int a,int b,char dep[]);

double ddg(double k[]);

int countline_pdb(FILE* fp);
void read_pdb(FILE* fp,node *atoms);
double readen();

void wlpdb(FILE* fname, node atom);
void terch(FILE* fname,char chain[1],char res[3],int atnum, int resnum);
void write_apo(double G[5],int N,node *atoms);
void write_h1(double G[5],int N,node *atoms);
void write_pdb(char fname[],double G[5],int N,node *atoms);
//-------

//Seed&Struct utilities
int spache_seed(node *atoms, int i);
void ratmpos(int i,int j,node *atoms,gsl_rng *r);
double rbfac(gsl_rng *r);
void gen_data(node *atoms,int i,gsl_rng *r);

int spache(node *atoms, int i,int N);
void connections(int **indexs,int N,node *atoms);
int cycle(int **indexs,int N,node *atoms);

int ep_alloc(ep *p,int n);
int ep_copy(ep *p1,ep *p2);
//----------------

//MC utilities
int aprob(double G0,double G1,double T,int i,gsl_rng *r);
double Temp(char filename[99]);

//moves
int HandOfGod(node *atoms,int N,int het,double xyzo[3],int **indexs,gsl_rng* r);
//----

//Diagnostic utilities
double lig_dist(node *atoms,int N);
double m_centre(node *atoms,int N);
void dsytrd3(double A[3][3], double Q[3][3], double d[3], double e[2]);
int dsyevq3(double A[3][3], double Q[3][3], double w[3]);
void inertia_t(ep *p);//calculate axes
int lig_com(ep *p);
int ep_trans_com(ep *p);
//----------------------------------

//avo
void make_seed(FILE *runs,char dir[],int N,double sdist,double G[5],node *atoms,int **indexs);
void samples(int n,int N,double sdist,int het);
int monte(int N,double sdist,int het,int co,ep *p1);
//-------


#endif
