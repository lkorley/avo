CC=gcc
CFLAGS=-lm -I.
DEPS = lib.h
OBJ = avo.o lib.o


inertia: avo
	gcc -std=c99 -o inertia inertia.c -lm

%.o: %.c$(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

avo: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)
