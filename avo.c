#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <ctype.h>
#include "lib.h"


//depenedencies: all

void make_seed(FILE *runs,char dir[],int N,double sdist,double G[5],node *atoms,int **indexs)
//generate random structure and save to dir as d.pd,
{
    printf("\n----------------------\nGenerating seed Structure\n");

    double ac=0.5*sdist/sqrt(3);//stores site co-ords
    char path[50];
    int i=0,d=0,cyc=0;
    char fname[20]="h2.pdb";
    gsl_rng *r=gsl_rng_alloc(rngtype);
    int s=time(NULL);
    gsl_rng_set(r,s);
    //double rdist=urand(r,0,ac-1);
    /*x[i]=(double)rsign()*urand(0,rdist); nrdist(rdist,x[i]);
    y[i]=(double)rsign()*urand(0,rdist); nrdist(rdist,y[i]);
    z[i]=(double)rsign()*urand(0,rdist);
    getres(res[i]);
    bfac[i]=rbfac();
    cpystr(atm[i],"ATOM",4);
    atoms[i].atnum=N+1;
    atoms[i].resnum=N;
    cpystr(atoms.[i]name,"CA",2);
    cpystr(atoms[i]chain,"A",1);
    cpystr(atoms[i].elem,"C",1);
    occ[i]=1.00;
    i++;*/
    //first ligand
    cpystr(atoms[i].atm,"HETATM",6);
    atoms[i].atnum=N+1;
    atoms[i].resnum=N;
    cpystr(atoms[i].res,"LIG",3);
    cpystr(atoms[i].name,"CA",2);
    cpystr(atoms[i].chain,"A",1);
    cpystr(atoms[i].elem,"C",1);
    atoms[i].occ=1.00;
    atoms[i].bfac=urand(r,15,20);
    atoms[i].x=-ac;atoms[i].y=-ac;atoms[i].z=-ac;
    i++;
    //second ligand
    cpystr(atoms[i].atm,"HETATM",6);
    atoms[i].atnum=N+1;
    atoms[i].resnum=N;
    cpystr(atoms[i].res,"LIG",3);
    cpystr(atoms[i].name,"CA",2);
    cpystr(atoms[i].chain,"A",1);
    cpystr(atoms[i].elem,"C",1);
    atoms[i].occ=1.00;
    atoms[i].bfac=urand(r,15,20);
    atoms[i].x=ac;atoms[i].y=ac;atoms[i].z=ac;

    while(cyc!=1)
    {
        for(i=2;i<(N);i++)//allocates details
        {
            ratmpos(i,-1,atoms,r);
            gen_data(atoms,i,r);
        }
        connections(indexs,N,atoms);
        //printf("\nconnectivity check %d\n",cyc);
        cyc=cycle(indexs,N,atoms);
        //printf("\nconnectivity check %d\n",cyc);
    }

    write_pdb(fname,G,N,atoms);
    sprintf(path,"%s/0.pdb",dir);
    copy("h2.pdb",path);

    fprintf(runs,"%-6d %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf\n",d,G[0],G[1],G[2],G[3],G[4]);//calculate allosteric free energy and save in seperate file
    printf("\nSeed structure complete\n----------------\n");

    gsl_rng_free(r);
}

void samples(int n,int N,double sdist,int het)
{
    double xyzo[3],G0,G[5];
    int d=1,k,i=0,pa;
    char patha[60],acc[20]="sample";
    time_t t0,t1;
    int d_t,tip=420;

    srand((unsigned)time(NULL));//seed generator

    FILE *runsa;
    mkdir(acc,S_IRWXU);
    runsa=fopen("sample/sampleruns.txt","w");
    if(runsa==NULL)//checking for success
    {
        printf("\nRun data storage files could not be created\nSTOP");
        exit(0);
    }
    fprintf(runsa,"#         G0          G11          G12          G2           DDG\n");

    t0=time(NULL);


    int **indexs;

    //make dynamic arrays
    indexs=malloc(N * sizeof *indexs);
    node *atoms;
    atoms=calloc(N,sizeof atoms);
    if(atoms==NULL || indexs==NULL)
    {
        printf("\nMemory could not be allocated for line data array\nSTOP");
        exit(0);
    }

    for(k=0;k<N;k++)
    {
        indexs[k]=malloc((N+1) * sizeof *indexs[k]);
        if(indexs[k]==NULL)
        {
            printf("\nMemory could not be allocated for line data arrays\nSTOP");
            exit(0);
        }
    }


    //OPTION FOR SEED FROM SAMPLE
    make_seed(runsa,"sample",N,sdist,G,atoms,indexs);

    k=0;
    connections(indexs,N,atoms);
    gsl_rng *r=gsl_rng_alloc(rngtype);
    int s=time(NULL);
    gsl_rng_set(r,s);
    while(d<0.5*n)
    {
        t1=time(NULL);
        d_t=(int)difftime(t1,t0);
        if(d_t%tip==0){gsl_rng_set(r,t1);}//reseed generator
        k=HandOfGod(atoms,N,het,xyzo,indexs,r);
        if(k>N){exit(0);}
        sprintf(patha,"sample/%d.pdb",d);
        write_pdb(patha,G,N,atoms);
        fprintf(runsa,"%-6d %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf\n",d,G[0],G[1],G[2],G[3],G[4]);
        pa=aprob(G0,G[4],1,-1,r);//check for acceptance
        if(pa!=0)
        {
            atoms[k].x=xyzo[0];
            atoms[k].y=xyzo[1];
            atoms[k].z=xyzo[2];
        }
        d+=1;
    }
    FILE *out;
    out=fopen("sample/0.pdb","r");
    read_pdb(out,atoms);
    fclose(out);
    while(d<n)
    {
        t1=time(NULL);
        d_t=(int)difftime(t1,t0);
        if(d_t%tip==0){gsl_rng_set(r,t1);}//reseed generator
        k=HandOfGod(atoms,N,het,xyzo,indexs,r);
        if(k>N){exit(0);}
        sprintf(patha,"sample/%d.pdb",d);
        write_pdb(patha,G,N,atoms);
        fprintf(runsa,"%-6d %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf\n",d,G[0],G[1],G[2],G[3],G[4]);
        pa=aprob(G0,G[4],1,1,r);//check for acceptance
        if(pa!=0)
        {
            atoms[k].x=xyzo[0];
            atoms[k].y=xyzo[1];
            atoms[k].z=xyzo[2];
        }
        d+=1;
    }

    printf("\n-------------------------------------------\nsample Complete\n");
    fclose(runsa);
    free(atoms);
    for(i=0;i<N;i++)
    {
        free(indexs[i]);
    }
    free(indexs);
    gsl_rng_free(r);
}

int monte(int N,double sdist,int het,int co,ep *p1)
{
    double T,cool=0.95,sig,cdist,dist,msqrt;
    double xyzo[3],G0,G[5],Nm[3],Gmax,cdistbest;
    int d=1,io=0,k,j=0,pa,count=0,i=0,best;
    char patha[60],acc[20]="runs";
    time_t t0,t1;
    int d_t,tip=420;

    srand((unsigned)time(NULL));

    FILE *runsa;
    mkdir(acc,S_IRWXU);
    runsa=fopen("runs.txt","w");
    if(runsa==NULL)//checking for success
    {
        printf("\nRun data storage files could not be created\nSTOP");
        exit(0);
    }
    fprintf(runsa,"#         G0          G11          G12          G2           DDG         T        Msqr    C.O.M   LigDist\n");

    t0=time(NULL);

    int **indexs;

    //make dynamic arrays
    indexs=malloc(N * sizeof *indexs);

    //if(x==NULL || y==NULL || z==NULL  || occ==NULL || bfac==NULL || atnum==NULL ||  resnum==NULL ||  atm==NULL ||  name==NULL ||  res==NULL ||  elem==NULL ||  chain==NULL || indexs==NULL)
    if(indexs==NULL)
    {
        printf("\nMemory could not be allocated for line data array\nSTOP");
        exit(0);
    }

    for(k=0;k<N;k++)
    {
        indexs[k]=malloc((N+1) * sizeof *indexs[k]);
        //if(atm[k]==NULL || name[k]==NULL || res[k]==NULL  || elem[k]==NULL  || chain[k]==NULL || indexs[k]==NULL)
        if(indexs[k]==NULL)
        {
            printf("\nMemory could not be allocated for line data arrays\nSTOP");
            exit(0);
        }
    }


    node *atoms;
    atoms=calloc(N,sizeof(atoms));
    //OPTION FOR SEED FROM SAMPLE
    make_seed(runsa,"runs",N,sdist,G,atoms,indexs);
    enms(j);j++;
    G0=G[4];
    Gmax=G0;
    T=Temp("sample/runs.txt");
    Nm[0]=0;Nm[1]=0;Nm[2]=0;

    gsl_rng *r=gsl_rng_alloc(rngtype);
    int s=time(NULL);
    gsl_rng_set(r,s);

    ep p2;
    ep_alloc(&p2,N);
    connections(indexs,N,atoms);

    while(T>fabs(Gmax*1e-4))
    {
        t1=time(NULL);
        d_t=(int)difftime(t1,t0);
        if(d_t%tip==0){gsl_rng_set(r,t1);}

        io=HandOfGod(atoms,N,het,xyzo,indexs,r);
        write_pdb("h2.pdb",G,N,atoms);
        pa=aprob(G0,G[4],T,co,r);//check for acceptance

        if(pa!=0)
        {
            atoms[io].x=xyzo[0];
            atoms[io].y=xyzo[1];
            atoms[io].z=xyzo[2];
            if(count>50){printf("\nTerminated after %d unsuccessful move attempts\n",count);break;}
            count++;
        }
        else
        {
            sprintf(patha,"runs/%d.pdb",j);//check for folder
            copy("h2.pdb",patha);
            cdist=m_centre(atoms,N);
            dist=lig_dist(atoms,N);
            fprintf(runsa,"%-6d %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf %12.6lf %8.3lf %8.3lf\n",j,G[0],G[1],G[2],G[3],G[4],T,msqrt,cdist,dist);
            if((co*G[4])>(co*Gmax))
            {
                Gmax=G[4];
                p2.atoms=atoms;
                memcpy(p2.G,G,sizeof(G));
                p2.T=T; p2.N=N; p2.iters=j;
                ep_copy(p1,&p2);
            }
            enms(j); j++;

            Nm[0]+=co*(G[4]-G0);
            Nm[1]+=SQR(G[4]-G0);
            Nm[2]+=1;
            sig=sqrt((Nm[1]-(SQR(Nm[0])/Nm[2]))/(Nm[2]-1));//variance calculation
            msqrt=sqrt(Nm[1])/Nm[2];
            if(Nm[2]>=10 && msqrt<T)//when 10 or more accepted
            {
                Nm[0]=0;
                Nm[1]=0;
                Nm[2]=0;
                T*=cool;
                if(sig==0){break;}
            }
            G0=G[4];
            count=0;
        }
    }


    printf("\n-------------------------------------------\nTest Complete\n");
    fclose(runsa);
    free(atoms);
    for(i=0;i<N;i++)
    {
        free(indexs[i]);
    }
    free(indexs);
    free(p2.atoms);
    return j;
}


