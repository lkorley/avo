----------------------------------------------------
AVO
Toy Model Network Optimization SCHEME
----------------------------------------------------
To install:
1. make
2. Enjoy



This program assumes DDPT toolbox is installed and available at /usr/local/DDPT/. 
This toolbox is available at: https://sourceforge.net/projects/durham-ddpt/

AVO:
The starting point for a protein evolution scheme. Uses a simple markov chain monte-carlo scheme to evolve structure of a network, optimising for DDG(allosteric free energy). Simulated annealing is employed to improve convergence. The network is evolved until 50 consecutive states are proposed without acceptance. The accepted structures are stored in the/runs folder and the runs.txt file lists the energies, DDG, Temperature, Meansquare accepted transition, ligand distance, and distance between ligand centre of mass and structure centre of mass.
axes.txt lists the length and projection of principal axes onto ligand seperation vector.

This is free software; you may redistribute it and/or modify it to your hearts content. Hopefully it is useful.

Any problems with the code please contact luke.korley@gmail.com



USAGE:
./avo -N numberofnodes -sd sitedistance [-var] [-aco] [-sam samplesize] [-tot]

Option    Type       Value           Description                         
------------------------------------------------------------             
 -N       Input                      number of nodes in network                       
 -sd      Input                      Cut-off for spring connectivity     
 -var      Opt                       Allow binding sites to move                    
 -tot      Opt          1            set total number of runs
 -sam    Input,Opt  samplesize       generate a sample
 -aco      Opt                       model anti-cooperativity

 

