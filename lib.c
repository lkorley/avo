#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <ctype.h>
#include "lib.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double SQR(double x)
{
    return x*x;
}

double rad(double x,double y,double z)
{
    return sqrt(SQR(x)+SQR(y)+SQR(z));
}

//-------------------------
//random utils

double urand(gsl_rng *r,double a, double b)
//uniform distribution [a,b)
{
    return gsl_rng_uniform(r)*(b-a) +a;
}


void nrdist(double rdist, double cord)
//calculate remaining distance
{
    rdist=sqrt(SQR(rdist)-SQR(cord));
}



