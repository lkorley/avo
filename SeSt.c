#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <ctype.h>
#include "lib.h"

//--------------------------------

//seed utils
//dependencies: random utils, read utils

int spache_seed(node *atoms, int i)
//check occupation of x,y,z
{
    int j=0,bools=0,k=0,l=0;//0 is empty 1 is filled
    double dist;
    for(j=0;j<i;j++)
    {
        //if(x[j]!=x[j]){continue;}
        dist=SQR(atoms[i].x-atoms[j].x)+SQR(atoms[i].y-atoms[j].y)+SQR(atoms[i].z-atoms[j].z);
        if(sqrt(dist)<=A_CUT)//filled if within sqrt(3) units of another residue
        {
            bools=1;
            break;
        }
    }
    //if(bools==1){printf("\nSpace occupied...\tRetrying...");}
    return bools;
}


void ratmpos(int i,int j,node *atoms, gsl_rng *r)
//generate random atom position
{
    int u=1;
    switch(j)
    {
        case -1:j=floor(urand(r,0,i));break;
        default:break;
    }
    double rdist=urand(r,3,M_CUT);
    double theta,phi;

    while(u==1)
    {
    phi=urand(r,0,2*M_PI);
    theta=acos(1- 2*(gsl_rng_uniform(r)));
    atoms[i].x=atoms[j].x+ sin(theta)*cos(phi);
    //nrdist(rdist,atoms[i].x);

    atoms[i].y=atoms[j].y+sin(theta)*sin(phi);
    //nrdist(rdist,atoms[i].y);

    atoms[i].z=atoms[j].z+cos(theta);

    u=spache_seed(atoms,i);
    }
}

double rbfac(gsl_rng *r)
//random bfactor
{
    return urand(r,20,70);
}

void gen_data(node *atoms,int i,gsl_rng *r)
//generates data for the new atom
{
    //getres(atoms[i].res);
    cpystr(atoms[i].res,"TOY",3);
    atoms[i].bfac=rbfac(r);
    cpystr(atoms[i].atm,"ATOM",4);
    atoms[i].atnum=atoms[i-1].atnum+1;
    atoms[i].resnum=atoms[i-1].resnum+1;
    cpystr(atoms[i].name,"CA",2);
    cpystr(atoms[i].chain,"A",1);
    cpystr(atoms[i].elem,"C",1);
    atoms[i].occ=1.00;
}

//----------------------------

//constraint utils
//dependeancies: random utils, read utils

int spache(node *atoms, int i,int N)
//check occupation of x,y,z
{
    int j,bools=0,het;//0 is empty 1 is filled
    double dist;
    if(ainb("HETATM",atoms[i].atm)==0)
    {
        het=0;
    }
    for(j=0;j<N;j++)
    {
        if(i==j){continue;}
        //dist=SQR(x[i]-x[j])+SQR(y[i]-y[j])+SQR(z[i]-z[j]);
        dist=rad(atoms[i].x-atoms[j].x,atoms[i].y-atoms[j].y,atoms[i].z-atoms[j].z);
        if(het==0 && ainb("HETATM",atoms[j].atm)==0 && dist<=M_CUT)
        {
            bools=1;
            printf("\nLigands too close\n");
            break;
        }
        if(dist<A_CUT)//filled if within cut units of another residue
        {
            bools=1;
            printf("\nspache %5lf\n",dist);
            printf("j= %4d | x= %5.2lf | y= %5.2lf | z= %5.2lf\n",j,atoms[j].x,atoms[j].y,atoms[j].z);
            printf("rn= %4d | x= %5.2lf | y= %5.2lf | z= %5.2lf\n",i,atoms[i].x,atoms[i].y,atoms[i].z);
            break;
        }
    }
    //if(bools==1){printf("\nSpace occupied...\tRetrying...");}
    return bools;
}

void connections(int **indexs,int N,node *atoms)
//stores their indices of connected nodes in index
//no connections to ligand sites recorded
//assumes ligands at end
{
    int i=0,count,a=0;
    int size,j=0;
    double dist;
    for(a=0;a<N;a++)
    {
        count=1;
        //if(ainb("HETATM",atm[a])==0){continue;}//stops connection to ligands
        //printf("%i\t",a);
        indexs[a]=realloc(indexs[a],(N) * sizeof *indexs[a]);
        for(i=0;i<N;i++)
        {

            //if(i==a || ainb("HETATM",atm[a])==0){continue;}//stops connection to self or ligands
            if(i==a){continue;}
            dist=SQR(atoms[i].x-atoms[a].x)+SQR(atoms[i].x-atoms[a].x)+SQR(atoms[i].x-atoms[a].x);
            if(dist<=SQR(M_CUT))
            {
                indexs[a][count]=i;
                //printf("%i\t",indexs[a][count]);
                count++;
            }
        }
        indexs[a]=realloc(indexs[a],count * sizeof *indexs[a]);
        indexs[a][0]=count;
        //printf("count= %d\tsize=%d\n",count,size);
    }
}

int cycle(int **indexs,int N,node *atoms)
//returns 1 if connected
{

    int *node,*cycles,ligo[2]={0,0},lcon=0,val;//0 is unvisited 1 is visited
    //N-=2;
    node=calloc(N,sizeof *node);
    cycles=calloc(N,sizeof *cycles);
    //memset(node,0,sizeof node);
    //memset(cycles,0,sizeof cycles);
    int visits=0,i=0,j=0,c=1,k,n=1,cur=n,ocycle,chkr;
    int size,count=0;
    /*cycles[i]=1;
    node[i]=1;
    //i=j;
    visits++;
    j=i;*/
    while(visits<=N)//terminates when N visits made
    {
        //printf("\nnode %d",j);
        size=indexs[i][0];
        if(ainb("HETATM",atoms[j].atm)==0)
        {
            c++;//seek new node
            if(c<size)
            {
                j=indexs[i][c];
                continue;
            }
            chkr=0;
        }
        //else if(i==j){chkr=1;}
        else{chkr=node[j];}
        //printf("\nnode check %i\n",j);

        if(chkr==1)//if j already visited
        {
            ocycle=cycles[j];
            //printf("\ncycle check full %d\n",j);
            if(ocycle!=cur && i!=j)//if not in current cycle
            {
                if(ocycle<cur)//visited pre-dates current cycle
                {
                    for(k=0;k<N;k++)//absorb current cycle
                    {
                        if(cycles[k]==cur && cycles[k]!=0)
                        {
                            cycles[k]=ocycle;
                        }
                    }
                    cur=ocycle;
                }
                else if(ocycle>cur)//visited postdates current cycle
                {
                    for(k=0;k<N;k++)//add to current cycle
                    {
                        if(cycles[k]==ocycle)
                        {
                            cycles[k]=cur;
                        }
                    }
                }
            }
            c++;
            if(c<size)//ensures no overflow
            {
                j=indexs[i][c];
                continue;
            }
        }

        if(c>=size)//no more available nodes in this cycle, start new
        {
            //check for connection to old cycles
            c=1;
            n++;
            cur=n;
            i=0;//looking for starting point for new cycle
            k=node[i];
            while(k==1 && i<N-1)
            {
                i++;
                if(ainb("HETATM",atoms[i].atm)==0){continue;}
                k=node[i];
            }
            if(k==1){/*printf("\njail break!! %d\n",visits);*/break;}
            //printf("\nnew cyc %d %d!!\n",i,indexs[i][0]);
            j=i;
            if(indexs[i][0]<=1)
            {
                node[j]=1;
                cycles[j]=cur;
            }
            //printf("\ncycle check new %d\n",i);
            continue;
            //i is starting point for new cycle
        }
        c=1;
        //printf("\n%d-%d\n",i,j);
        node[j]=1;
        cycles[j]=cur;
        visits++;
        i=j;
        j=indexs[i][c];
        //printf("\ncycle check %d\n",visits);
    }
    //checking ligands connected
    for(k=0;k<N;k++)
    {
        if(ainb("HETATM",atoms[k].atm)==0)
        {
            if(indexs[k][0]>1)
            {
                ligo[lcon]=1; lcon++;
                //printf("\nligs check %d\n",indexs[k][0]);
            }
        }
    }

    for(k=0;k<N;k++)
    {
        count+=cycles[k];
        if(cycles[k]!=1 && ainb("HETATM",atoms[k].atm)!=0)//not all in 1 cycle
        {
            //printf("\nloop check %d\n",k);
            count=0;
            break;
        }
        //if(ainb("HETATM",atm[k])!=0){printf("\nloop check %d\n",count);}
    }
    count+=ligo[0]+ligo[1];
    //printf("\ncyc check %d\n",count);
    free(node);free(cycles);
    return count/N;
}

//------------------------------------

int ep_alloc(ep *p,int n)
{
    p->atoms=malloc(n*sizeof(node));
    if(p->atoms==NULL){return 1;}
    p->N=n;
    return 0;
}


int ep_copy(ep *p1,ep *p2)
//copies p2 into p1 (overwrites & resizes where necessary)
{
    if(p2->N!=p1->N){p1->atoms=realloc(p1->atoms,sizeof(*(p2->atoms)));}
    if(p1->atoms==NULL){return 1;}
    p1->atoms=memcpy(p1->atoms,p2->atoms,sizeof((*p2->atoms)));

    p1->N=p2->N;p1->iters=p2->iters;p1->T=p2->T;
    memcpy(p1->G,p2->G,sizeof(p2->G));
    return 0;
}

int ep_trans_com(ep *p)
{
    double com[3]={0};
    for(int i=0;i<p->N;i++)
    {
        com[0]+=(p->atoms[i].x)/(double)p->N;
        com[1]+=(p->atoms[i].y)/(double)p->N;
        com[2]+=(p->atoms[i].z)/(double)p->N;
    }
    for(int i=0;i<p->N;i++)
    {
        p->atoms[i].x-=com[0];
        p->atoms[i].y-=com[1];
        p->atoms[i].z-=com[2];
    }
    return 0;
}

